﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APITest
{
    public partial class RepoInfo : Form
    {
        //private Repository repo;
        public RepoInfo(Repository repo)
        {
            InitializeComponent();
           // this.repo = repo;
            nametb.Text = repo.name;
            idtb.Text = repo.id.ToString();
            nodetb.Text = repo.node_id;
            fullnametb.Text = repo.full_name;
            desctb.Text = repo.description;
            linkonrepo.Text = repo.html_url;

        }

        private void linkonrepo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("chrome",linkonrepo.Text);
        }
    }
}
