﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace APITest
{
    public partial class Form1 : Form
    {
        private HttpClient httpClient = new HttpClient();
        private const string GithubApi = "https://api.github.com";
        public Form1()
        {
            InitializeComponent();

            //defaults for httpClient
            httpClient.DefaultRequestHeaders.Add("User-agent", "Any");

        }


        private async void getInfoBtn_Click(object sender, EventArgs e)
        {
            var userName = usernameTb.Text;

            var requestUrl = $"{GithubApi}/users/{userName}";

            var userResponse = await httpClient.GetAsync(requestUrl);

            var jsonUserResp = await userResponse.Content.ReadAsStringAsync();

            var _user = JsonConvert.DeserializeObject<User>(jsonUserResp);

            logintxt.Text = _user.login;
            idtxt.Text = _user.id.ToString();
            nametxt.Text = _user.name;
            typetxt.Text = _user.type;
            locationtxt.Text = _user.location;
            createdtxt.Text = _user.created_at;
            linkLabel1.Text = _user.html_url;

            avatatbox.ImageLocation = _user.avatar_url;


            await LoadReposAsync(_user.repos_url);          


        }

        private async Task LoadReposAsync(string repos_url)
        {
            //work with repos
            var reposResponce = await httpClient.GetAsync(repos_url);

            var jsonRepResponse = await reposResponce.Content.ReadAsStringAsync();


            var repos = JsonConvert.DeserializeObject<List<Repository>>(jsonRepResponse);


            listBox1.Items.Clear();
            foreach (var repo in repos)
            {
                listBox1.Items.Add(repo);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var selectedRepo = listBox1.SelectedItem as Repository;
            if (selectedRepo == null) return;

            var repInfoForm  = new RepoInfo(selectedRepo) ;
            repInfoForm.Show();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("chrome", linkLabel1.Text);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occured while starting chrome with url. Message: {ex.Message}");                
            }
        }
    }

    public class User 
    {
        public  string login { get; set; }

        public int id { get; set; }

        public string name { get; set; }

        public string type { get; set; }

        public string location { get; set; }

        public string created_at { get; set; }

        public string avatar_url { get; set; }

        public string html_url { get; set; }

        public string repos_url { get; set; }
    }

    public class Repository
    {
        public int id { get; set; }

        public string name { get; set; }

        public string node_id { get; set; }

        public string full_name { get; set; }

        public string html_url { get; set; }

        public string description { get; set; }


        public override string ToString()
        {
            return $"{id} {name}";
        }
    }
}
