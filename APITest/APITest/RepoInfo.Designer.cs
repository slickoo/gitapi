﻿namespace APITest
{
    partial class RepoInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nametb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.linkonrepo = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.idtb = new System.Windows.Forms.TextBox();
            this.nodetb = new System.Windows.Forms.TextBox();
            this.fullnametb = new System.Windows.Forms.TextBox();
            this.desctb = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label1.Location = new System.Drawing.Point(22, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Repo name:";
            // 
            // nametb
            // 
            this.nametb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.nametb.Location = new System.Drawing.Point(174, 25);
            this.nametb.Name = "nametb";
            this.nametb.ReadOnly = true;
            this.nametb.Size = new System.Drawing.Size(373, 26);
            this.nametb.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label2.Location = new System.Drawing.Point(22, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Repo id:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label3.Location = new System.Drawing.Point(22, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Repo node:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label4.Location = new System.Drawing.Point(22, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Full name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label5.Location = new System.Drawing.Point(22, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Description:";
            // 
            // linkonrepo
            // 
            this.linkonrepo.AutoSize = true;
            this.linkonrepo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.linkonrepo.Location = new System.Drawing.Point(171, 428);
            this.linkonrepo.Name = "linkonrepo";
            this.linkonrepo.Size = new System.Drawing.Size(84, 20);
            this.linkonrepo.TabIndex = 7;
            this.linkonrepo.TabStop = true;
            this.linkonrepo.Text = "linkLabel1";
            this.linkonrepo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkonrepo_LinkClicked);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label6.Location = new System.Drawing.Point(22, 428);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 20);
            this.label6.TabIndex = 8;
            this.label6.Text = "Link on repo:";
            // 
            // idtb
            // 
            this.idtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.idtb.Location = new System.Drawing.Point(174, 84);
            this.idtb.Name = "idtb";
            this.idtb.ReadOnly = true;
            this.idtb.Size = new System.Drawing.Size(373, 26);
            this.idtb.TabIndex = 9;
            // 
            // nodetb
            // 
            this.nodetb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.nodetb.Location = new System.Drawing.Point(174, 136);
            this.nodetb.Name = "nodetb";
            this.nodetb.ReadOnly = true;
            this.nodetb.Size = new System.Drawing.Size(373, 26);
            this.nodetb.TabIndex = 10;
            // 
            // fullnametb
            // 
            this.fullnametb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.fullnametb.Location = new System.Drawing.Point(174, 204);
            this.fullnametb.Name = "fullnametb";
            this.fullnametb.ReadOnly = true;
            this.fullnametb.Size = new System.Drawing.Size(373, 26);
            this.fullnametb.TabIndex = 11;
            // 
            // desctb
            // 
            this.desctb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.desctb.Location = new System.Drawing.Point(174, 272);
            this.desctb.Multiline = true;
            this.desctb.Name = "desctb";
            this.desctb.ReadOnly = true;
            this.desctb.Size = new System.Drawing.Size(373, 139);
            this.desctb.TabIndex = 12;
            // 
            // RepoInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 490);
            this.Controls.Add(this.desctb);
            this.Controls.Add(this.fullnametb);
            this.Controls.Add(this.nodetb);
            this.Controls.Add(this.idtb);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.linkonrepo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nametb);
            this.Controls.Add(this.label1);
            this.Name = "RepoInfo";
            this.Text = "RepoInfo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nametb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkonrepo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox idtb;
        private System.Windows.Forms.TextBox nodetb;
        private System.Windows.Forms.TextBox fullnametb;
        private System.Windows.Forms.TextBox desctb;
    }
}